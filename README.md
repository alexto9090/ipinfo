# IPINFO
Get Network provider info.



# Simply add to your projects using to your project level gradle. 

allprojects {
    repositories { 
			maven { url 'https://jitpack.io' }
		}
	}

# and below one in your app level gradle.

 dependencies {  
			implementation 'com.gitlab.alexto9090:ipinfo:1.0'
}

